import React, { useState, useEffect } from 'react';
import EventChart from './Components/chart';
import fetchData from './Apicalls/Fetchcall'
export default function App() {
  const [visitors, setVisitors] = useState({ data: [], status: "INIT" })
  const [gender, setGenders] = useState({ data: [], status: "INIT" })
  const [type, setType] = useState("")
  function formSubmit(e) {
    e.preventDefault();
    setType(e.target.type.value)
  }
  useEffect(() => {
    const check = async () => {
      if (type) {
        setVisitors({ data: [], status: "LOADING" })
        const data = await fetchData(`/api/events?type=` + type);
        setVisitors(data);
      }
    }
    check();
  }, [type])
  useEffect(() => {
    const check = async () => {
      setGenders({ data: [], status: "LOADING" })
      const data = await fetchData(`/api/gender`);
      setGenders(data);
    }
    check();
  }, [])
  return (
    <>
      <h2> Submit the form to get stats</h2>
      <form onSubmit={e => formSubmit(e)}>
        <label> visitors <input type="radio" name="type" value="visitors"></input></label>
        <label>Staff<input type="radio" name="type" value="staff"></input></label>
        <button type="submit">submit</button>
      </form>
      {type !== "" && <h2>The  stats of {type}</h2>}<br />
      {visitors.status !== "INIT" && <EventChart response={visitors} />}
      <h2>Gender wise Stats</h2>
      {gender.status !== "INIT" && <EventChart response={gender} type="Gender" />}

    </>
  );
}