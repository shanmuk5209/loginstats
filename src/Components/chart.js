import React from 'react';
import Chart from './BarChart';
const EventChart = ({ response, type }) => {
    return (
        <div>
            {response.status === "ERROR" && "Something went wrong"}
            {response.status === "LOADING" && "Loading ..."}
            {response.status === "SUCCESS" && response.data.length === 0 && "No data found"}
            {response.status === "SUCCESS" && response.data.length > 0 && <Chart type={type} data={response.data} />}

        </div>
    );
}
export default EventChart;
