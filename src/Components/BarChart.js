import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, } from 'recharts';
const Chart = ({ data, type }) => {
    return (
        <BarChart
            width={900}
            height={300}
            data={data}
            margin={{
                top: 5, right: 30, left: 20, bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="hour" interval={0} />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey={(type === "Gender") ? 'men' : 'in'} fill="#8884d8" />
            <Bar dataKey={(type === "Gender") ? 'women' : 'out'} fill="#82ca9d" />
        </BarChart>)
}
export default Chart