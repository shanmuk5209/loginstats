export default async function fetchData(url) {
    let result={data:[],status:"INIT"}
    result.status="LOADING";
    await fetch(url)
        .then(res => {
            if (!res.ok) throw Error(res.statusText)
            return res.json();
        })
        .then((data) => { return data.map(e => ({ ...e, hour: new Date(e.time).toLocaleTimeString([], { hour: '2-digit' }) })); })
        .then(data => {
            result.data=data;
            result.status="SUCCESS";
            return result
        })
        .catch(error => {
            console.log('error :', error);
            result.status="ERROR";
            return result
        })
        return result
}