module.exports = function(app) {
    app.get("/api/visitors", (req, res) => {
        const now = Date.now();
        const rand = Math.random();
        if (rand < 0.4) {
            res.json(
                Array.from({ length: 24 }).map((_, i) => ({
                        visitorIn: Math.floor(Math.random() * 100),
                        visitorOut: Math.floor(Math.random() * 100),
                        staffIn: Math.floor(Math.random() * 100),
                        staffOut: Math.floor(Math.random() * 100),
                        time: now + (i * 1 * 60 * 60 * 1000) 
                    })
                ))
        }

        if(rand >= 0.4 && rand < 0.6) {
            setTimeout(() => {
                res.json(
                    Array.from({ length: 24 }).map((_, i) => ({
                            visitorIn: Math.floor(Math.random() * 100),
                            visitorOut: Math.floor(Math.random() * 100),
                            staffIn: Math.floor(Math.random() * 100),
                            staffOut: Math.floor(Math.random() * 100),
                            time: now + (i * 1 * 60 * 60 * 1000) 
                        })
                    ))
            }, 5000)
        }

        if(rand >= 0.6 && rand < 0.85) {
            res.status(500).end()
        }

        if(rand >= 0.85) {
            res.json([])
        }
    })
    app.get("/api/events", (req, res) => {
        const now = Date.now();
        const rand = Math.random();
        if (rand < 0.4) {
            res.json(
                Array.from({ length: 24 }).map((_, i) => ({
                        in: Math.floor(Math.random() * 100),
                        out: Math.floor(Math.random() * 100),
                        time: now + (i * 1 * 60 * 60 * 1000)
                    })
                ))
        }

        if(rand >= 0.4 && rand < 0.6) {
            setTimeout(() => {
                res.json(
                    Array.from({ length: 24 }).map((_, i) => ({
                            in: Math.floor(Math.random() * 100),
                            out: Math.floor(Math.random() * 100),
                            time: now + (i * 1 * 60 * 60 * 1000) 
                        })
                    ))
            }, 5000)
        }

        if(rand >= 0.6 && rand < 0.85) {
            res.status(500).end()
        }

        if(rand >= 0.85) {
            res.json([])
        }
    })
    app.get("/api/gender", (req, res) => {
        const now = Date.now();
        const rand = Math.random();
        if (rand < 0.4) {
            res.json(
                Array.from({ length: 24 }).map((_, i) => ({
                        men: Math.floor(Math.random() * 100),
                        women: Math.floor(Math.random() * 100),
                        time: now + (i * 1 * 60 * 60 * 1000)
                    })
                ))
        }

        if(rand >= 0.4 && rand < 0.6) {
            setTimeout(() => {
                res.json(
                    Array.from({ length: 24 }).map((_, i) => ({
                            men: Math.floor(Math.random() * 100),
                            women: Math.floor(Math.random() * 100),
                            time: now + (i * 1 * 60 * 60 * 1000) 
                        })
                    ))
            }, 5000)
        }

        if(rand >= 0.6 && rand < 0.85) {
            res.status(500).end()
        }

        if(rand >= 0.85) {
            res.json([])
        }
    })
    
  };